#include <PinChangeInt.h>

#define CONTROL0 5    
#define CONTROL1 4
#define CONTROL2 3
#define CONTROL3 2
#define PED_BUTTON  6
#define DELAY0 9
#define DELAY1 10
#define DELAY2 11
#define DELAY3 12
#define NORTH_LTS 18
#define SOUTH_LTS 17
//Light states
#define RED      0
#define YELLOW   1
#define GREEN    2
#define ADVANCE  3
#define LEFT 4
//Traffic states
#define ALLRED   -1
#define NSGREENLONG  0
#define NSGREEN  1
#define NSYELLOW 2
#define EWGREENLONG 3
#define EWGREEN  4
#define EWYELLOW 5
#define NADVANCE 6
#define SADVANCE 7
#define NSLEFT   8
//Pedestrian States
#define WALK     0
#define DONTWALK 1
#define FLASH    2
//Wait values
#define  NOWAIT 0
#define  WAIT   1
#define  N_LTS  1
#define  S_LTS  2
#define  B_LTS  3
#define  FLASH_RATE 500 //ms
#define  LTS_SENS  1012

#define TRUE 1
#define FALSE 0
//Varibles
int LED_State_Traffic[14];
int LED_State_Pedestrian[8];
int northLTSval;
int southLTSval;
int mainDelay;
short lastState;
short nextState;
short pedWait;
short ltsWait;
short pedFlashNS;
short pedFlashEW;
unsigned long lastFlash;
unsigned long lastChange;

void setup(){
  //Set I/O pins to correct settings
  pinMode(CONTROL0, OUTPUT);
  pinMode(CONTROL1, OUTPUT);
  pinMode(CONTROL2, OUTPUT);
  pinMode(CONTROL3, OUTPUT);
  pinMode(NORTH_LTS, INPUT);
  pinMode(SOUTH_LTS, INPUT);
  pinMode(DELAY0, INPUT); digitalWrite(DELAY0, HIGH);
  pinMode(DELAY1, INPUT); digitalWrite(DELAY1, HIGH);
  pinMode(DELAY2, INPUT); digitalWrite(DELAY2, HIGH);
  pinMode(DELAY3, INPUT); digitalWrite(DELAY3, HIGH);
  pinMode(PED_BUTTON, INPUT); digitalWrite(PED_BUTTON, HIGH);
  //Assign interrupts
  PCintPort::attachInterrupt(PED_BUTTON, &pedWaitInterrupt, RISING);
  PCintPort::attachInterrupt(DELAY0, &delayChange, CHANGE);
  PCintPort::attachInterrupt(DELAY1, &delayChange, CHANGE);
  PCintPort::attachInterrupt(DELAY2, &delayChange, CHANGE);
  PCintPort::attachInterrupt(DELAY3, &delayChange, CHANGE);
  Serial.begin(9600); //for debugging
  
  //Set initial values
  northLTSval = 0;
  southLTSval = 0;
  pedWait = NOWAIT;
  ltsWait = NOWAIT;
  changeState(ALLRED);
  lastState = ALLRED;
  nextState = NSGREEN;

  delayChange();
  
}


void loop(){
  //Check for left turn
  pollLTS();
  cycle();
  // Prep for MUX2
  pinMode(16, OUTPUT);
  pinMode(15, INPUT);
  for (int i=0; i<14; i++){
    digitalWrite(CONTROL0, (i&15)>>3); //S3
    digitalWrite(CONTROL1, (i&7)>>2);  //S2
    digitalWrite(CONTROL2, (i&3)>>1);  //S1
    digitalWrite(CONTROL3, (i&1));     //S0 
    digitalWrite(16, LED_State_Traffic[i]);
    delayMicroseconds(750);
  }
  //Prep for MUX1
  pinMode(16, INPUT);
  pinMode(15, OUTPUT);
  for (int i=0; i<8; i++){
    digitalWrite(CONTROL0, (i&15)>>3); //S3
    digitalWrite(CONTROL1, (i&7)>>2);  //S2
    digitalWrite(CONTROL2, (i&3)>>1);  //S1
    digitalWrite(CONTROL3, (i&1));     //S0
    unsigned long currentTime = millis();
    if((currentTime - lastFlash > FLASH_RATE) && (pedFlashNS || pedFlashEW)){
       if (pedFlashNS){
           LED_State_Pedestrian[0] = !LED_State_Pedestrian[0];
           LED_State_Pedestrian[2] = !LED_State_Pedestrian[2];          
         } else {
           LED_State_Pedestrian[4] = !LED_State_Pedestrian[4];
           LED_State_Pedestrian[6] = !LED_State_Pedestrian[6];
         }
      lastFlash = currentTime;
    } 
    digitalWrite(15, LED_State_Pedestrian[i]);
    delayMicroseconds(750);
  }
}

void pedWaitInterrupt(){
  pedWait = WAIT;
}

void delayChange(){
  mainDelay = 0;
  for (int i = 0; i<4; i++)
    if (digitalRead(DELAY0 + i) == LOW)
      mainDelay += bit(i);   
  mainDelay *= 1000;
}

void cycle(){
  unsigned long currentTime = millis();
  if(currentTime - lastChange > mainDelay || (lastState == NSGREENLONG && pedWait != NOWAIT)){    
    switch(lastState){
    case NSYELLOW:
      changeState(ALLRED);
      lastState = ALLRED;
      nextState = EWGREENLONG;
      ltsWait = NOWAIT;
      break;
    case EWYELLOW:
      changeState(ALLRED);
      lastState = ALLRED;
      if (ltsWait == B_LTS)
        nextState = NSLEFT;
      else if (ltsWait == N_LTS)
        nextState = NADVANCE;
      else if (ltsWait == S_LTS)
        nextState = SADVANCE;
      else
        nextState = NSGREENLONG;
      break;
    case NADVANCE:
      changeState(NSGREENLONG);
      lastState = NSGREENLONG;
      break;
    case SADVANCE:
      changeState(NSGREENLONG);
      lastState = NSGREENLONG;
      break;
    case NSGREENLONG:
      changeState(NSGREEN);
      lastState = NSGREEN;
      break;
    case NSGREEN:
      pedWait = NOWAIT;
      changeState(NSYELLOW);
      lastState = NSYELLOW;
      break;
    case EWGREENLONG:
      pedWait = NOWAIT;
      changeState(EWGREEN);
      lastState = EWGREEN;
      break;
    case EWGREEN:
      changeState(EWYELLOW);
      lastState = EWYELLOW;
      break;
    case NSLEFT:
      changeState(NSGREENLONG);
      lastState = NSGREENLONG;
      break;
    default:
      changeState(nextState);
      lastState = nextState;     
    }
    lastChange = currentTime;
  }
}

void pollLTS(){
  northLTSval = analogRead(NORTH_LTS);
  southLTSval = analogRead(SOUTH_LTS);
  Serial.println(northLTSval);
  Serial.println(southLTSval);
  if (northLTSval > LTS_SENS && southLTSval < LTS_SENS && ltsWait != B_LTS){
      ltsWait = N_LTS;
  } else if (southLTSval > LTS_SENS && northLTSval < LTS_SENS && ltsWait != B_LTS){
      ltsWait = S_LTS;
  } else if (northLTSval > LTS_SENS && southLTSval > LTS_SENS){
      ltsWait = B_LTS;
  }
}

void changeState(int state){
  switch(state){
  case NSGREENLONG:
    northLight(GREEN);
    southLight(GREEN);
    eastLight(RED);
    westLight(RED);
    eastPed(WALK);
    westPed(WALK);
    northPed(DONTWALK);
    southPed(DONTWALK);
    break;
  case NSGREEN:
    northLight(GREEN);
    southLight(GREEN);
    eastLight(RED);
    westLight(RED);
    eastPed(DONTWALK);
    westPed(DONTWALK);
    northPed(DONTWALK);
    southPed(DONTWALK);
    pedFlashEW = TRUE;
    break;
  case NSYELLOW:
    northLight(YELLOW);
    southLight(YELLOW);
    eastLight(RED);
    westLight(RED);
    eastPed(DONTWALK);
    westPed(DONTWALK);
    northPed(DONTWALK);
    southPed(DONTWALK);
    pedFlashEW = FALSE;
    break;
  case EWGREENLONG:
    northLight(RED);
    southLight(RED);
    eastLight(GREEN);
    westLight(GREEN);
    eastPed(DONTWALK);
    westPed(DONTWALK);
    northPed(WALK); 
    southPed(WALK);
    break;
  case EWGREEN:
    northLight(RED);
    southLight(RED);
    eastLight(GREEN);
    westLight(GREEN);
    eastPed(DONTWALK);
    westPed(DONTWALK);
    northPed(DONTWALK);
    southPed(DONTWALK);
    pedFlashNS = TRUE;
    break;
  case EWYELLOW:
    northLight(RED);
    southLight(RED);
    eastLight(YELLOW);
    westLight(YELLOW);
    eastPed(DONTWALK);
    westPed(DONTWALK);
    northPed(DONTWALK);
    southPed(DONTWALK);
    pedFlashNS = FALSE;
    break;
  case NADVANCE:
    northLight(ADVANCE);
    southLight(RED);
    eastLight(RED);
    westLight(RED);
    eastPed(WALK);
    westPed(DONTWALK);
    northPed(DONTWALK);
    southPed(DONTWALK);
    break;
  case SADVANCE:
    northLight(RED);
    southLight(ADVANCE);
    eastLight(RED);
    westLight(RED);
    eastPed(DONTWALK);
    westPed(WALK);
    northPed(DONTWALK);
    southPed(DONTWALK); 
    break;
  case NSLEFT:
    northLight(LEFT);
    southLight(LEFT);
    eastLight(RED);
    westLight(RED);
    eastPed(DONTWALK);
    westPed(DONTWALK);
    northPed(DONTWALK);
    southPed(DONTWALK);
    break;
  default :
    northLight(RED);
    southLight(RED);
    eastLight(RED);
    westLight(RED);
    eastPed(DONTWALK);
    westPed(DONTWALK);
    northPed(DONTWALK);
    southPed(DONTWALK);
    break;
  }

}


void northLight(int state){
  switch(state){
  case RED :
    LED_State_Traffic[0] = HIGH;
    LED_State_Traffic[1] = LOW;
    LED_State_Traffic[2] = LOW;
    LED_State_Traffic[3] = LOW;
    break;
  case YELLOW :
    LED_State_Traffic[0] = LOW;
    LED_State_Traffic[1] = HIGH;
    LED_State_Traffic[2] = LOW;
    LED_State_Traffic[3] = LOW;
    break;
  case GREEN :
    LED_State_Traffic[0] = LOW;
    LED_State_Traffic[1] = LOW;
    LED_State_Traffic[2] = HIGH;
    LED_State_Traffic[3] = LOW;
    break;
  case ADVANCE :
    LED_State_Traffic[0] = LOW;
    LED_State_Traffic[1] = LOW;
    LED_State_Traffic[2] = HIGH;
    LED_State_Traffic[3] = HIGH;
    break;
  case LEFT:
    LED_State_Traffic[0] = LOW;
    LED_State_Traffic[1] = LOW;
    LED_State_Traffic[2] = LOW;
    LED_State_Traffic[3] = HIGH;
    break;
  default :
    LED_State_Traffic[0] = LOW;
    LED_State_Traffic[1] = LOW;
    LED_State_Traffic[2] = LOW;
    LED_State_Traffic[3] = LOW;
    break;
  }
}

void southLight(int state){
  switch(state){
  case RED :
    LED_State_Traffic[4] = HIGH;
    LED_State_Traffic[5] = LOW;
    LED_State_Traffic[6] = LOW;
    LED_State_Traffic[7] = LOW;
    break;
  case YELLOW :
    LED_State_Traffic[4] = LOW;
    LED_State_Traffic[5] = HIGH;
    LED_State_Traffic[6] = LOW;
    LED_State_Traffic[7] = LOW;
    break;
  case GREEN :
    LED_State_Traffic[4] = LOW;
    LED_State_Traffic[5] = LOW;
    LED_State_Traffic[6] = HIGH;
    LED_State_Traffic[7] = LOW;
    break;
  case ADVANCE :
    LED_State_Traffic[4] = LOW;
    LED_State_Traffic[5] = LOW;
    LED_State_Traffic[6] = HIGH;
    LED_State_Traffic[7] = HIGH;
    break;
  case LEFT:
    LED_State_Traffic[4] = LOW;
    LED_State_Traffic[5] = LOW;
    LED_State_Traffic[6] = LOW;
    LED_State_Traffic[7] = HIGH;
    break;
  default:
    LED_State_Traffic[4] = LOW;
    LED_State_Traffic[5] = LOW;
    LED_State_Traffic[6] = LOW;
    LED_State_Traffic[7] = LOW;
    break;
  }
}
void eastLight(int state){
  switch(state){
  case RED :
    LED_State_Traffic[8] = HIGH;
    LED_State_Traffic[9] = LOW;
    LED_State_Traffic[10] = LOW;
    break;
  case YELLOW :
    LED_State_Traffic[8] = LOW;
    LED_State_Traffic[9] = HIGH;
    LED_State_Traffic[10] = LOW;
    break;
  case GREEN :
    LED_State_Traffic[8] = LOW;
    LED_State_Traffic[9] = LOW;
    LED_State_Traffic[10] = HIGH;
    break;
  default :
    LED_State_Traffic[8] = LOW;
    LED_State_Traffic[9] = LOW;
    LED_State_Traffic[10] = LOW;
    break;
  }
}
void westLight(int state){
  switch(state){
  case RED :
    LED_State_Traffic[11] = HIGH;
    LED_State_Traffic[12] = LOW;
    LED_State_Traffic[13] = LOW;
    break;
  case YELLOW :
    LED_State_Traffic[11] = LOW;
    LED_State_Traffic[12] = HIGH;
    LED_State_Traffic[13] = LOW;
    break;
  case GREEN :
    LED_State_Traffic[11] = LOW;
    LED_State_Traffic[12] = LOW;
    LED_State_Traffic[13] = HIGH;
    break;
  default:
    LED_State_Traffic[11] = LOW;
    LED_State_Traffic[12] = LOW;
    LED_State_Traffic[13] = LOW;
    break;
  }
}

void northPed(int state){
  unsigned long currentTime = millis();
  switch (state){
    case WALK:
      LED_State_Pedestrian[0] = LOW;
      LED_State_Pedestrian[1] = HIGH;
      break;
    case DONTWALK:
      LED_State_Pedestrian[0] = HIGH;
      LED_State_Pedestrian[1] = LOW;
      break;
    default:
      LED_State_Pedestrian[0] = LOW;
      LED_State_Pedestrian[1] = LOW;
      break;
  }
}
void southPed(int state){
  unsigned long currentTime = millis();
  switch (state){
    case WALK:
      LED_State_Pedestrian[2] = LOW;
      LED_State_Pedestrian[3] = HIGH;
      break;
    case DONTWALK:
      LED_State_Pedestrian[2] = HIGH;
      LED_State_Pedestrian[3] = LOW;
      break;
    default:
      LED_State_Pedestrian[2] = LOW;
      LED_State_Pedestrian[3] = LOW;
      break;
  }
}
void eastPed(int state){
  switch (state){
    case WALK:
      LED_State_Pedestrian[4] = LOW;
      LED_State_Pedestrian[5] = HIGH;
      break;
    case DONTWALK:
      LED_State_Pedestrian[4] = HIGH;
      LED_State_Pedestrian[5] = LOW;
      break;
    default:
      LED_State_Pedestrian[4] = LOW;
      LED_State_Pedestrian[5] = LOW;
      break;
  }
}
void westPed(int state){
  switch (state){
    case WALK:
      LED_State_Pedestrian[6] = LOW;
      LED_State_Pedestrian[7] = HIGH;
      break;
    case DONTWALK:
      LED_State_Pedestrian[6] = HIGH;
      LED_State_Pedestrian[7] = LOW;
      break;
    default:
      LED_State_Pedestrian[6] = LOW;
      LED_State_Pedestrian[7] = LOW;
      break;
  }
}
  
